package com.javarush.task.task21.task2113;

import java.util.ArrayList;
import java.util.List;

public class Hippodrome {

    static Hippodrome game;

    private List<Horse> horses;

    public Hippodrome(List<Horse> horses) {
        this.horses = horses;
    }

    public List<Horse> getHorses() {
        return horses;
    }

    public void move() {
        for (int i = 0; i < horses.toArray().length; i++) {
            getHorses().get(i).move();
        }
    }

    public void run() {
        for (int i = 1; i <= 100; i++) {
            move();
            print();
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void print() {
        for (int i = 0; i < horses.toArray().length; i++)
            getHorses().get(i).print();
        for (int k = 0; k < 10; k++)
            System.out.println();
    }

    public Horse getWinner() {
        Horse winner = getHorses().get(0);
        for (Horse horse : getHorses()) {
            if (horse.getDistance() > winner.getDistance())
                winner = horse;
        }
        return winner;
    }

    public void printWinner() {
        System.out.println("Winner is " + getWinner().getName() + "!");
    }

    public static void main(String[] args) {
        List<Horse> horses = new ArrayList<>();
        horses.add(new Horse("First", 3.0, 0.0));
        horses.add(new Horse("Second", 3.0, 0.0));
        horses.add(new Horse("Third", 3.0, 0.0));
        game = new Hippodrome(horses);
        game.run();
        game.printWinner();
    }
}
