package com.javarush.task.task05.task0507;

/* 
Среднее арифметическое
*/

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution {

    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        int number;
        double sum = 0;
        int counter = 0;
        while (true) {
            number = readNumber();
            if (number == -1) {
                System.out.println(sum / counter);
                break;
            }
            sum += number;
            counter++;

        }
    }

    private static int readNumber() throws IOException {
        return Integer.parseInt(
                new BufferedReader(new InputStreamReader(System.in))
                        .readLine());
    }
}

