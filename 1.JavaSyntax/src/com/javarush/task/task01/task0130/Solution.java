package com.javarush.task.task01.task0130;

/* 
Наш первый конвертер!
TC = (TF - 32) * 5/9
*/

public class Solution {
    public static void main(String[] args) {
        System.out.println(convertCelsiumToFahrenheit(41));
    }

    public static double convertCelsiumToFahrenheit(int celsium) {
        //напишите тут ваш код

        return (double) (9 * celsium) / 5 + 32;
    }
}