package com.javarush.task.task04.task0428;

/* 
Положительное число
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        int[] numbers = new int[3];
        int positiveNumbersCount = 0;

        for (int number : numbers) {
            number = readNumber();
            if (number > 0)
                positiveNumbersCount++;
        }

        System.out.println(positiveNumbersCount);
    }

    private static int readNumber() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        return Integer.parseInt(reader.readLine());
    }
}
