package com.javarush.task.task04.task0417;

/* 
Существует ли пара?
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        int a = readNumber();
        int b = readNumber();
        int c = readNumber();

        if (a == b && b != c)
            printNumbers(new int[]{a, b});
        else if (a == c && b != c)
            printNumbers(new int[]{a, c});
        else if (b == c && c != a)
            printNumbers(new int[]{b, c});
        else if (a == b && b == c)
            printNumbers(new int[]{a, b, c});
    }

    private static void printNumbers(int[] numbers) {
        for (int number : numbers)
            System.out.print(number + " ");
    }

    private static int readNumber() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        return Integer.parseInt(reader.readLine());
    }
}