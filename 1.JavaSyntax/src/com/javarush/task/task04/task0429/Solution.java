package com.javarush.task.task04.task0429;

/* 
Положительные и отрицательные числа
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        int[] numbers = new int[3];
        int positiveNumbersCount = 0;
        int negativeNumbersCount = 0;
        for (int number : numbers) {
            number = readNumber();
            if (number > 0)
                positiveNumbersCount++;
            else if (number < 0)
                negativeNumbersCount++;
        }
        System.out.println(
                String.format("количество отрицательных чисел: %d", negativeNumbersCount));
        System.out.println(
                String.format("количество положительных чисел: %d", positiveNumbersCount));
    }


    private static int readNumber() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        return Integer.parseInt(reader.readLine());
    }
}
