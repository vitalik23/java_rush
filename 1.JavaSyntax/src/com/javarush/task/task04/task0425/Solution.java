package com.javarush.task.task04.task0425;

/* 
Цель установлена!
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        int a = readNumber();
        int b = readNumber();
        System.out.println(checkQuoter(a, b));
    }


    public static int checkQuoter(int a, int b) {
        int quoter = 0;
        if (a > 0 && b > 0)
            quoter = 1;
        else if (a < 0 && b > 0)
            quoter = 2;
        else if (a < 0 && b < 0)
            quoter = 3;
        else if (a > 0 && b < 0)
            quoter = 4;
        return quoter;
    }

    private static int readNumber() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        return Integer.parseInt(reader.readLine());
    }
}
