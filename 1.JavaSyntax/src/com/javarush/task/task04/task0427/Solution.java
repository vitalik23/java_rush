package com.javarush.task.task04.task0427;

/* 
Описываем числа
*/


import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        int number = readNumber();
        checkNumber(number);
    }

    private static void checkNumber(int number) {

        if (number < 1000 && number >= 100) {

            if (number % 2 == 0)
                System.out.println("четное трехзначное число");
            else
                System.out.println("нечетное трехзначное число");

        } else if (number < 100 && number >= 10) {

            if (number % 2 == 0)
                System.out.println("четное двузначное число");
            else
                System.out.println("нечетное двузначное число");

        } else if (number < 10 && number > 0) {

            if (number % 2 == 0)
                System.out.println("четное однозначное число");
            else
                System.out.println("нечетное однозначное число");

        }
    }

    private static int readNumber() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        return Integer.parseInt(reader.readLine());
    }
}
