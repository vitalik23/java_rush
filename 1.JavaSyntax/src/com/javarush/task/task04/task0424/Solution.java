package com.javarush.task.task04.task0424;

/* 
Три числа
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        int[] numbers = new int[3];
        for (int i = 0; i < numbers.length; i++)
            numbers[i] = readNumber();

        if (numbers[1] == numbers[2])
            System.out.println(1);
        else if (numbers[0] == numbers[2])
            System.out.println(2);
        else if (numbers[0] == numbers[1])
            System.out.println(3);
    }

    private static int readNumber() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        return Integer.parseInt(reader.readLine());
    }
}
