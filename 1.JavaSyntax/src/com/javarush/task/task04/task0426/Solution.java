package com.javarush.task.task04.task0426;

/* 
Ярлыки и числа
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        int number = readNumber();
        checkTheNumber(number);
    }

    private static void checkTheNumber(int number) {
        if (number < 0) {
            if (number % 2 == 0)
                System.out.println("отрицательное четное число");
            else
                System.out.println("отрицательное нечетное число");
        } else if (number > 0) {
            if (number % 2 == 0)
                System.out.println("положительное четное число");
            else
                System.out.println("положительное нечетное число");
        } else
            System.out.println("ноль");
    }

    private static int readNumber() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        return Integer.parseInt(reader.readLine());
    }
}
