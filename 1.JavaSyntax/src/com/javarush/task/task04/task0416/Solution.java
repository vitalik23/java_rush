package com.javarush.task.task04.task0416;

/* 
Переходим дорогу вслепую
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        double minute = readNumber();
        if (minute > 60)
            checkColor((minute % 60) % 5);
        else if (minute >= 5 && minute <= 60)
            checkColor(minute % 5);
        else
            checkColor(minute);
    }


    private static double readNumber() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//        return Integer.parseInt(reader.readLine());
        return Double.parseDouble(reader.readLine());
    }

    private static void checkColor(double minute) {
        if (minute < 3)
            System.out.println("зелёный");
        else if (minute >= 3 && minute < 4)
            System.out.println("жёлтый");
        else if (minute < 5)
            System.out.println("красный");
    }

}


