package com.javarush.task.task04.task0415;

/* 
Правило треугольника
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int a = Integer.parseInt(reader.readLine());
        int b = Integer.parseInt(reader.readLine());
        int c = Integer.parseInt(reader.readLine());
        if (isTriangle(a, b, c))
            System.out.println("Треугольник существует.");
        else
            System.out.println("Треугольник не существует.");
    }

    private static boolean isTriangle(int a, int b, int c) {
        if (a < (b + c) && b < (a + c) && c < (a + b))
            return true;
        else
            return false;
    }
}