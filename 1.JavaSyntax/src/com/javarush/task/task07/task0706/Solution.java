package com.javarush.task.task07.task0706;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

/* 
Улицы и дома
*/

public class Solution {

    static int evenHousesPopulation = 0;
    static int oddHousesPopulation = 0;

    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        int[] houses = new int[15];

        fillArrayAndCountPopulation(houses);

        if (evenHousesPopulation < oddHousesPopulation)
            System.out.println("В домах с нечетными номерами проживает больше жителей.");
        else
            System.out.println("В домах с четными номерами проживает больше жителей.");
    }

    private static int[] fillArrayAndCountPopulation(int[] array) throws IOException {
        for (int i = 0; i < array.length; i++) {
            array[i] = readNumber();
            if (i % 2 != 0)
                oddHousesPopulation += array[i];
            else
                evenHousesPopulation += array[i];
        }
        return array;
    }

    private static int readNumber() throws IOException {
        return Integer.parseInt(new BufferedReader(new InputStreamReader(System.in)).readLine());
    }
}
