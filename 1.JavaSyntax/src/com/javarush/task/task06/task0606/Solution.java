package com.javarush.task.task06.task0606;

import java.io.*;

/* 
Чётные и нечётные циферки
*/

public class Solution {

    public static int even;
    public static int odd;

    public static void main(String[] args) throws IOException {
        //напишите тут ваш код

        int number = readInt();

        while (number != 0) {

            if (isLastNumberIsEven(number))
                even++;
            else
                odd++;

            number /= 10;
        }

        System.out.println(String.format("Even: %d Odd: %d", even, odd));
    }

    private static int readInt() throws IOException {
        return Integer.parseInt(new BufferedReader(new InputStreamReader(System.in)).readLine());
    }

    private static boolean isLastNumberIsEven(int number) {
        return (number % 10) % 2 == 0;
    }
}
