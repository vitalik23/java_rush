package com.javarush.test.level07.lesson06.task03;

/* 5 строчек в обратном порядке
1. Создай список строк.
2. Считай с клавиатуры 5 строк и добавь в него.
3. Расположи их в обратном порядке.
4. Используя цикл выведи содержимое на экран, каждое значение с новой строки.
*/

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        ArrayList<String> list = new ArrayList<String>();

        initializeList(list);
        reversList(list);
        printList(list);
    }

    public static void printList(ArrayList<String> list){
        for (int i = 0; i < list.size(); i++)
            System.out.println(list.get(i));
    }

    public static ArrayList<String> reversList(ArrayList<String> list){

        for (int i = 3; i >= 0; i--) {
            String Str = list.get(i);
            list.remove(i);
            list.add(Str);
        }

        return list;
    }

    public static void initializeList(ArrayList<String> list) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        for (int i = 0; i < 5; i++) {
            list.add(reader.readLine());
        }
    }
}
