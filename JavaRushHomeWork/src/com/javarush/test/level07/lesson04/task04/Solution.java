package com.javarush.test.level07.lesson04.task04;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

/* Массив из чисел в обратном порядке
1. Создать массив на 10 чисел.
2. Ввести с клавиатуры 10 чисел и записать их в массив.
3. Расположить элементы массива в обратном порядке.
4. Вывести результат на экран, каждое значение выводить с новой строки.
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        int size = 10;

        int[] array = new int[size];

        arrayInitialize(array);
        //System.out.println(Arrays.toString(array));
        printArray(array);


    }

    public static void arrayInitialize(int[] array) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            for (int i = 0; i < array.length; i++){
                array[i] = Integer.parseInt(reader.readLine());
            }

    }

    public static void printArray(int[] array){
        for (int i = array.length - 1; i >= 0; i--){
            System.out.println(array[i]);
        }
    }
}

