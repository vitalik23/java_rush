package com.javarush.test.level07.lesson04.task02;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

/* Массив из строчек в обратном порядке
1. Создать массив на 10 строчек.
2. Ввести с клавиатуры 8 строчек и сохранить их в массив.
3. Вывести содержимое всего массива (10 элементов) на экран в обратном порядке. Каждый элемент - с новой строки.
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        //напишите тут ваш код
        int arraySize = 10;

        String[] arrayOfStrings = new String[arraySize];

        initializeArray(arrayOfStrings);
        //System.out.println(Arrays.toString(arrayOfStrings));
        printArray(arrayOfStrings);

    }

    static void initializeArray(String[] array) throws IOException
    {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        for (int i = 0; i < array.length; i++)
        {
            array[i] = reader.readLine();
        }

    }

    static void printArray(String[] array)
    {
        for (int i = array.length -1; i >= 0; i--)
        {
            System.out.println(array[i]);
        }


    }
}