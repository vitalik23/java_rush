package com.javarush.test.level07.lesson04.task05;

/* Один большой массив и два маленьких
1. Создать массив на 20 чисел.
2. Ввести в него значения с клавиатуры.
3. Создать два массива на 10 чисел каждый.
4. Скопировать большой массив в два маленьких: половину чисел в первый маленький, вторую половину во второй маленький.
5. Вывести второй маленький массив на экран, каждое значение выводить с новой строки.
*/

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        int bigArraySize = 20;

        int[] bigArray = new int[bigArraySize];
        int[] firstSmallArray = new int[bigArraySize/2];
        int[] secondSmallArray = new int[bigArraySize/2];

        initializeArray(bigArray);

        //System.out.ln(Arrays.toString(bigArray));

        for (int i = 0; i < bigArray.length/2; i++){
            firstSmallArray[i] = bigArray[i];
        }

        for (int i = bigArray.length/2; i < bigArray.length; i++){
            secondSmallArray[i - (bigArray.length/2)] = bigArray[i];
        }

        printArray(secondSmallArray);
    }

    public static void initializeArray(int[] array) throws IOException
    {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        for (int i = 0; i < array.length; i++){
            array[i] = Integer.parseInt(reader.readLine());
        }

    }

    public static void printArray(int[] array){
        for (int i = 0; i < array.length; i++){
            System.out.println(array[i]);
        }
    }
}
