package com.javarush.test.level07.lesson04.task03;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* 2 массива
1. Создать массив на 10 строк.
2. Создать массив на 10 чисел.
3. Ввести с клавиатуры 10 строк, заполнить ими массив строк.
4. В каждую ячейку массива чисел записать длину строки из массива строк, индекс/номер ячейки которой совпадает
с текущим индексом из массива чисел. Вывести содержимое массива чисел на экран, каждое значение выводить с новой
строки.
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        int arraySize = 10;

        String[] arrayOfStrings = new String[arraySize];

        int[] arrayOfInts = new int[arraySize];

        initialiseArrays(arrayOfStrings, arrayOfInts);
        printArray(arrayOfInts);
    }

    static void printArray( int[] array){

        for (int i = 0; i < array.length; i++) {

            System.out.println(array[i]);

        }
    }

    static void initialiseArrays(String[] arrayS, int[] arrayI) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        for (int i = 0; i < arrayS.length; i++){
            arrayS[i] = reader.readLine();
            arrayI[i] = arrayS[i].length();
        }

    }
}
